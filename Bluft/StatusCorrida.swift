//
//  StatusCorrida.swift
//  Bluft
//
//  Created by Cristino Junior on 18/03/18.
//  Copyright © 2018 Cristino Junior. All rights reserved.
//

enum StatusCorrida: String {
    case EmRequisicao, PegarPassageiro, IniciarViagem, EmViagem, ViagemFinalizada
}
